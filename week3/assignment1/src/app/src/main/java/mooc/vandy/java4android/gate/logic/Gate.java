package mooc.vandy.java4android.gate.logic;

/**
 * This file defines the Gate class.
 */
public class Gate {
    // TODO -- Fill in your code here

    public static final int IN     = 1;
    public static final int OUT    = -1;
    public static final int CLOSED = 0;

    private int mSwing;

    public Gate() {
        this.mSwing = CLOSED;
    }

    public int getSwingDirection() {
        return this.mSwing;
    }

    public boolean setSwing(final int direction) {
        if ((direction == IN || direction == OUT || direction == CLOSED) && this.mSwing != direction) {
            this.mSwing = direction;
            return true;
        }
        return false;
    }

    public boolean open(final int direction) {
        setSwing(direction);
        return (direction == IN || direction == OUT);
    }

    public void close() {
        setSwing(CLOSED);
    }

    public int thru(final int count) {
        int result = 0;
        if (this.mSwing == IN) {
            result = count;
        } else if (this.mSwing == OUT) {
            result = -count;
        }
        return result;
    }

    @Override
    public String toString() {
        String state;
        switch (this.mSwing) {
            case CLOSED:
                state = "is closed";
                break;
            case IN:
                state = "is open and swings to enter the pen only";
                break;
            case OUT:
                state = "is open and swings to exit the pen only";
                break;
            default:
                state = "has an invalid swing direction";
        }
        return String.format("This gate %s", state);
    }
}
