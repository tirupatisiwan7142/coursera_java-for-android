package mooc.vandy.java4android.calculator.logic;

/**
 * Perform the Add operation.
 */
public class Add implements MathOperation {

    /**
     * Addition of two numbers
     *
     * @param valueA
     * @param valueB
     * @return addition of two numbers.
     */
    public String calc(int valueA, int valueB) {
        return String.valueOf(valueA + valueB);
    }
}
