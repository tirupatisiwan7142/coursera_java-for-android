package mooc.vandy.java4android.buildings.logic;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

/**
 * This is the office class file, it is a subclass of Building.
 */
public class Office extends Building {
    private String mBusinessName;
    private int    mParkingSpaces;

    private static int sTotalOffices = 0;

    public Office(final int length, final int width, final int lotLength, final int lotWidth) {
        super(length, width, lotLength, lotWidth);
        sTotalOffices++;
    }

    public Office(final int length, final int width, final int lotLength, final int lotWidth, String businessName) {
        this(length, width, lotLength, lotWidth);
        this.mBusinessName = businessName;
        this.mParkingSpaces = 0;
    }

    public Office(final int length, final int width, final int lotLength, final int lotWidth, final String businessName,
                  final int parkinsSpaces) {
        this(length, width, lotLength, lotWidth);
        this.mBusinessName = businessName;
        this.mParkingSpaces = parkinsSpaces;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(final String inMBusinessName) {
        this.mBusinessName = inMBusinessName;
    }

    public int getParkingSpaces() {
        return mParkingSpaces;
    }

    public void setParkingSpaces(final int inMParkingSpaces) {
        this.mParkingSpaces = inMParkingSpaces;
    }

    @Override
    public String toString() {
        ArrayList<String> result = new ArrayList<>();

        if (mBusinessName == null || mBusinessName.isEmpty()) {
            result.add("Business: unoccupied");
        } else {
            result.add(String.format("Business: %s", mBusinessName));
        }

        if (mParkingSpaces > 0) {
            result.add(String.format("has %s parking spaces", mParkingSpaces));
        }

        return StringUtils.join(result, "; ") + (sTotalOffices > 0 ?
                                                         String.format(" (total offices: %s)", sTotalOffices) : "");
    }


    @Override
    public boolean equals(final Object o) {
        Office that = (Office) o;
        return that.calcBuildingArea() == this.calcBuildingArea() && that.mParkingSpaces == this.mParkingSpaces;
    }
}
